import React, { useState, useEffect } from "react";
import axios from "axios";
import { toast } from 'react-toastify';
import { useNavigate } from "react-router-dom";


function AddStudent() {
    const [ firstName, setFirstName ] = useState("");
    const [ lastName, setLastName ] = useState("");
    const [ email, setEmail ] = useState("");
    const [ dob, setDob ] = useState("");
    const [ department, setDepartment ] = useState('');
    const [ departments, setDepartments ] = useState([]);

    const navigator = useNavigate();

    const fetchStudents = async () => {
      const { data } = await axios.get(`${process.env.REACT_APP_API_URL}/departments/`);
      setDepartments(data);
    }

    useEffect(() => {
      fetchStudents();
    })

    async function handleSubmit(e) {
        e.preventDefault();
        const student = { firstName, lastName, email, dob };
        try {
          const { data } = await axios.post(`${process.env.REACT_APP_API_URL}/students/${parseInt(department)}`, student);

          console.log('got res: ', data);
          toast.success(data, { position: toast.POSITION.TOP_CENTER, autoClose: 5000 });
          navigator("/");
        } catch( e ) {
          console.log(e);
        }
    }

    return (
        <div className="w-full bg-gray-500 p-3 h-screen"> 
            <form onSubmit = { handleSubmit } className = "mx-auto bg-white border rounded w-2/5 p-2 mt-3">
               <h1 className="text-center text-blue-400">ADD STUDENT</h1>
               <div className="block">
                 <label>First Name</label>
                 <input className="h-10 border rounded w-full block pl-1" type = "text" value = { firstName }
                 onChange = { e => setFirstName(e.target.value) } 
                 />
               </div>
               <div className="block">
                 <label>Last Name</label>
                 <input className="h-10 border rounded w-full block pl-1" type = "text" value = { lastName }
                 onChange = { e => setLastName(e.target.value) } 
                 />
               </div>
               <div className="block">
                 <label>Email</label>
                 <input className="h-10 border rounded w-full block pl-1" type = "text" value = { email }
                 onChange = { e => setEmail(e.target.value) } 
                 />
               </div>
               <div className="block">
                 <label>Date of Birth</label>
                 <input className="h-10 border rounded w-full block" type = "date" value = { dob }
                 onChange = { e => setDob(e.target.value) } 
                 /> 
               </div>
               <div className="block">
                 <label>Department</label>
                 <select name="department" id="department" className="h-10 border rounded w-full block"
                 onChange = { (e) => setDepartment(e.target.value) }>
                  <option value="0">Select department</option>
                  { departments.map(depart => <option key={ depart.id } value={ depart.id }> { depart.name } </option>) }
                 </select>
               </div>
               <div className="block">
                 <button className="w-28 h-12 bg-blue-400 text-white border rounded p-2 mt-2" type="submit"> Save </button>
               </div>
            </form>
        </div>
    )
}

export default AddStudent;