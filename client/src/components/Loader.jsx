import "../assets/main.css";

const Loader = () => {
  const nums = [1, 4, 6, 8, 9, 1];
  return (
    <div className="w-full p-1 flex justify-between flex-row flex-wrap h-screen">
      {nums.map((num, index) => (
        <div
          className="border shadow rounded-md p-4 w-1/4 m-1 bg-gray-300"
          key={index}
        >
          <div className="animate-pulse flex space-x-4">
            <div className="rounded-full bg-slate-700 h-10 w-10"></div>
            <div className="flex-1 space-y-6 py-1">
              <div className="h-2 bg-slate-700 rounded"></div>
              <div className="space-y-3">
                <div className="grid grid-cols-3 gap-4">
                  <div className="h-2 bg-slate-700 rounded col-span-2"></div>
                  <div className="h-2 bg-slate-700 rounded col-span-1"></div>
                </div>
                <div className="h-2 bg-slate-700 rounded"></div>
              </div>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
};

export default Loader;
