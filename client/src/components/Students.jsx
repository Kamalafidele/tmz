import React, { useState, useEffect } from "react";
import { Link } from 'react-router-dom';
import axios from "axios";
import Loader from "./Loader";



function Student() {
  const [ students, setStudents ] = useState([]);
  const [ isLoading, setIsLoading ] = useState(false);


  const fetchStudents = async () => {
    setIsLoading(true);
    try {
      const { data } = await axios.get(`${process.env.REACT_APP_API_URL}/students/`);
      setStudents(data);
    } catch (e) {
      console.log('Error: ', e);
    }
    setIsLoading(false);
  };

  useEffect(() => {
    fetchStudents();
  }, []);

  const deleteItem = async (id) => {
       setStudents(students.filter(student => student.id !== id));
       await axios.delete(`${process.env.REACT_APP_API_URL}/students/delete/${id}`);
  }

  return (
    <div className="w-full h-full p-3">
      <Link className="bg-blue-600 border rounded text-white p-2 relative mb-2" to={"/add_student"}>Add Student</Link>
      { isLoading ? (
        <Loader />
      ) : (

        <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400 p-3 mt-3">
          <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
              <th scope="col" className="px-6 py-3">First Name</th>
              <th scope="col" className="px-6 py-3">Last Name</th>
              <th scope="col" className="px-6 py-3">Email</th>
              <th scope="col" className="px-6 py-3">Age</th>
              <th scope="col" className="px-6 py-3">DOB</th>
              <th scope="col" className="px-6 py-3" colSpan = {2}>Action</th>
            </tr>
          </thead>
          <tbody>
            { students.map((student, index) =>
              <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700" key={index}>
                <td className="px-6 py-4"> { student.firstName } </td>
                <td className="px-6 py-4"> { student.lastName } </td>
                <td className="px-6 py-4"> { student.email } </td>
                <td className="px-6 py-4"> { student.age } </td>
                <td className="px-6 py-4"> { student.dob } </td>
                <td className="px-6 py-4 text-yellow-400 cursor-pointer"> Edit </td>
                <td className="px-6 py-4 text-red-300 cursor-pointer" onClick = { () => deleteItem(student.id) }> Delete </td>
              </tr>
            ) }
          </tbody>
        </table>

      )
      }
    </div >
  );
}

export default Student;
