import React, { useState } from 'react';

const Test = (props) =>{
    const [ text, setText ] = useState("");


    return (
        <>
          <div>
             <h1>KAMARA - TESTING</h1>
             <input type = "text" value = { text } className = "w-1/4 border border-solid border-blue-300"  
             onChange = {(e) => setText(e.target.value)}/>
             <button className = "w-1/4 bg-blue-400 text-white rounded p-2 mt-2"
              onClick = {() => props.getData(text) }
             >TESTING</button>
          </div>
        </>
    )
}

export default Test;