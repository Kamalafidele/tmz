import { Route, Routes } from "react-router-dom";
import Student from "./components/Students";
import AddStudent from "./components/AddStudent";
import EditStudent from "./components/EditStudent";
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function App() {
  return (
    <>
    <ToastContainer />
      <Routes>
        <Route path="/" element = { <Student/> } />
        <Route path="/add_student" element = { <AddStudent/> } />
        <Route path="/edit_student/:student_id" element = { <EditStudent/> } />
      </Routes>
    </>
  );
}

export default App;
