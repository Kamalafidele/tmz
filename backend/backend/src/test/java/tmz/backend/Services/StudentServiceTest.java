package tmz.backend.Services;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import tmz.backend.Models.Department;
import tmz.backend.Models.Student;
import tmz.backend.Repos.StudentRepo;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;

@RunWith(MockitoJUnitRunner.class)
public class StudentServiceTest {
    @Mock
    private StudentRepo studentRepoMock;

    @InjectMocks
    private StudentService studentService;

    Department department = department = new Department(1, "Applied Mathematics");;
    List<Student> studentList = Arrays.asList(new Student(1L,"kamara", "fidele", "kamala@gmail.com", LocalDate.parse("2004-05-23"), department), new Student(2L,"Mugisha", "Orreste", "mugisha@gmail.com", LocalDate.parse("2010-10-13"), department));

    @Test
    public void getAll() {
        when(studentRepoMock.findAll()).thenReturn(studentList);
        assertEquals("kamala@gmail.com", studentService.listOfStudents().get(0).getEmail());
        assertEquals(2, studentService.listOfStudents().size());
    }

    @Test
    public void create_success() {
        when(studentRepoMock.save(any(Student.class))).thenReturn(studentList.get(1));

        assertEquals("Mugisha", studentService.createStudent(studentList.get(0)).getFirstName());
    }
}

