package tmz.backend.Controllers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import tmz.backend.Models.Department;
import tmz.backend.Models.Student;
import tmz.backend.Services.StudentService;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(StudentController.class)
public class StudentControllerTest {
    @MockBean
    private StudentService studentServiceMock;
//    @Autowired
    private MockMvc mockMvc;

    @Test
    public void get_all_success() throws Exception {
        Department department = new Department(1, "Applied Mathematics");
        List<Student> studentList = Arrays.asList(new Student("kamara", "fidele", "kamala@gmail.com", LocalDate.parse("2004-05-23"), department));
        when(studentServiceMock.listOfStudents()).thenReturn(studentList);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/api/v1/students/")
                .accept(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc
                .perform(request)
                .andExpect(status().isOk())
                .andExpect(content().json("[{\"id\":1,\"firstName\":\"kamara\",\"lastName\":\"fidele\", \"email\": \"kamala@gmail.com\", \"dob\": \"2004-05-23\",\"age\": \"18\",\"department\": \"{\"id\": 1, \"name\": \"Applied Mathematics\" }\" }]"))
                .andReturn();
    }
}
