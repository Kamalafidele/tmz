package tmz.backend.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tmz.backend.Models.Department;
import tmz.backend.Repos.DepartmentRepo;

import java.util.List;
import java.util.Optional;

@Service
public class DepartmentService {
    @Autowired
    private DepartmentRepo departmentRepo;

    public DepartmentService() {}

    public List<Department> listAllDepartments() {
        return departmentRepo.findAll();
    }
    public Optional<Department> getDepartment(int id) {
        return this.departmentRepo.findById(id);
    }

}
