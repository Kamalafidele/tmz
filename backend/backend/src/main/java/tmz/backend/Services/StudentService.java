package tmz.backend.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import tmz.backend.Models.Student;
import tmz.backend.Repos.StudentRepo;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class StudentService {
    @Autowired
    private StudentRepo studentRepo;

    public StudentService() {
    }

    public List<Student> listOfStudents() {
        return studentRepo.findAll();
    }

    public Optional<Student> getStudent(Long id) {
        return studentRepo.findById(id);
    }

    public Student createStudent(Student student) {
        return studentRepo.save(student);
    }

    public void deleteStudent(Long id) {
        studentRepo.deleteById(id);
    }

    public Student getByEmail(String email) {
        return studentRepo.findByEmail(email).get();
    }

    public List<Student> search(String key){
        return studentRepo.findByFirstNameLike(key);
    }

    public Page<Student> getAllStudent(Integer pageNo, Integer pageSize, String sortBy){
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        return studentRepo.findAll(paging);
    }

    public Page<Student> listStudentsByPage() {
        Sort sort=Sort.by(Sort.Direction.ASC, "firstName").and(Sort.by(Sort.Direction.DESC,"lastName"));
        PageRequest pageRequest=PageRequest.of(0, 10,sort);
        Page<Student> page = studentRepo.findAll(pageRequest);
        return page;
    }

    public List<Student> getAllStudents(Integer pageNo, Integer pageSize, String sortBy)
    {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

        Page<Student> pagedResult = studentRepo.findAll(paging);

        if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<Student>();
        }
    }
}
