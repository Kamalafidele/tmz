package tmz.backend.Repos;

import org.springframework.data.jpa.repository.JpaRepository;
import tmz.backend.Models.Student;

import java.util.List;
import java.util.Optional;

public interface StudentRepo extends JpaRepository<Student, Long> {
    public Optional<Student> findByEmail(String email);

    public List<Student> findByFirstNameLike(String key);
}
