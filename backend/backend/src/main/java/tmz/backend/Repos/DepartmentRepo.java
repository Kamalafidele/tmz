package tmz.backend.Repos;

import org.springframework.data.jpa.repository.JpaRepository;
import tmz.backend.Models.Department;

public interface DepartmentRepo extends JpaRepository<Department, Integer> {

}
