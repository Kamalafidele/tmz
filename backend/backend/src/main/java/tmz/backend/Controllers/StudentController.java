package tmz.backend.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tmz.backend.Dtos.StudentDto;
import tmz.backend.Models.Department;
import tmz.backend.Models.Student;
import tmz.backend.Services.DepartmentService;
import tmz.backend.Services.StudentService;

import java.util.*;

@RestController
@RequestMapping("/api/v1/students")
@CrossOrigin("*")
public class StudentController {
    @Autowired
    private StudentService studentService;
    @Autowired
    private DepartmentService departmentService;

    @GetMapping("/")
    public List<Student> getStudents() {
        List<Student> students = studentService.listOfStudents();
        return students;
    }

    @GetMapping("/{student_id}")
    public Optional<Student> getStudent(@PathVariable("student_id") Long studentId) {
        Optional<Student> student = studentService.getStudent(studentId);
        return student;
    }

    @PostMapping("/{departmentId}")
    public ResponseEntity<String> addStudent(@RequestBody StudentDto dto, @PathVariable("departmentId") int departmentId) {

        Optional<Department> department = this.departmentService.getDepartment(departmentId);
        Student student = new Student(dto.getFirstName(), dto.getLastName(), dto.getEmail(), dto.getDob(), department.get());

        this.studentService.createStudent(student);
        return new ResponseEntity<>("Student added successfully", HttpStatus.CREATED);
    }

    @PutMapping("/edit/{student_id}")
    public ResponseEntity<Map> updateStudent(@PathVariable("student_id") Long studentId, @RequestBody Student body) {
        Map<String, Object> response = new HashMap<>();
        Optional<Student> optionalStudent = studentService.getStudent(studentId);

        if (optionalStudent.isEmpty()) {
            response.put("error", "No student with " + studentId + " id was found");
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

        Student student = studentService.createStudent(body);

        response.put("message", "Student updated successfully");
        response.put("student", student);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{student_id}")
    public ResponseEntity<String> deleteStudent(@PathVariable("student_id") Long studentId) {
        studentService.deleteStudent(studentId);

        return new ResponseEntity<>("Student deleted successfully", HttpStatus.OK);
    }

    @GetMapping("/email/{student_email}")
    public Student getStudentByEmail(@PathVariable("student_email") String email) {
        return studentService.getByEmail(email);
    }

    @GetMapping("/search/{key}")
    public List<Student> searchStudents(@PathVariable("key") String key) {
        return studentService.search(key);
    }

    @GetMapping("/pages")
    @ResponseStatus(HttpStatus.OK)
    public Page<Student> getPages(
            @RequestParam(defaultValue = "0") Integer pageNo,
            @RequestParam(defaultValue = "10") Integer pageSize,
            @RequestParam(defaultValue = "email") String sortBy) {
        return studentService.getAllStudent(pageNo, pageSize, sortBy);
    }

    @GetMapping("/students/pageone")
    @ResponseStatus(HttpStatus.OK)
    public Page<Student> studentPagination() {
        return studentService.listStudentsByPage();

    }
}
