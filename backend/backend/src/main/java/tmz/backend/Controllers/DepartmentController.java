package tmz.backend.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tmz.backend.Models.Department;
import tmz.backend.Services.DepartmentService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/departments")
@CrossOrigin("*")
public class DepartmentController {
    @Autowired
    private DepartmentService departmentService;

    @GetMapping("/")
    public List<Department> getDepartments() {
        return this.departmentService.listAllDepartments();
    }

    @GetMapping("/{departmentId}")
    public Optional<Department> getDepartment(@PathVariable("departmentId") int departmentId) {
        return this.departmentService.getDepartment(departmentId);
    }
}
