package tmz.backend.Models;


import java.time.LocalDate;
import java.time.Period;
import javax.persistence.*;

@Entity
public class Student {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "firstName", nullable = false, length = 100)
    private String firstName;
    @Column(name = "lastName", nullable = false, length = 50)
    private String lastName;
    @Column(name = "email", length = 100, unique = true)
    private String email;
    private LocalDate dob;
    @Transient
    private int age;

    @ManyToOne
    @JoinColumn(name = "did", nullable = false)
    private Department department;

    public Student() {
        super();
    }

    public Student(String firstName, String lastName, String email, LocalDate dob, Department department) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.dob = dob;
        this.department = department;
    }

    public Student(Long id,String firstName, String lastName, String email, LocalDate dob,Department department ) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.dob = dob;
        this.department = department;
    }

    public Student(Long id, String firstName, String lastName, String email, LocalDate dob, int age) {
        super();
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.dob = dob;
        this.age = age;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public LocalDate getDob() {
        return dob;
    }
    public void setDob(LocalDate dob) {
        this.dob = dob;
    }
    public int getAge() {
        LocalDate date = LocalDate.now();
        if(this.getDob()!=null) {
            return Period.between(this.dob, date).getYears();
        }
        else {
            return 0;
        }
    }
    public void setAge(int age) {
        this.age = age;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    @Override
    public String toString() {
        return "Student [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
                + ", dob=" + dob + ", age=" + age + "]";
    }
}
