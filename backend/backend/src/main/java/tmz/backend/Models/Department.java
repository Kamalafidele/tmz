package tmz.backend.Models;

import javax.persistence.*;

@Entity
@Table
public class Department {
    @Id
    private int id;
    @Column(nullable = true, length = 40)
    private String name;

    public Department() {}

    public Department(String name) {
        this.name = name;
    }

    public Department(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
