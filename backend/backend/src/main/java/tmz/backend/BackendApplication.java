package tmz.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.context.annotation.ImportResource;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@ImportResource({"classpath*:ApplicationContext.xml"})
public class BackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendApplication.class, args);
	}

//	@Bean
//	CommandLineRunner commandLineRunner(StudentRepo studentRepo, DepartmentRepo departmentRepo){
//       return args -> {
//		   Faker faker = new Faker();
//		   for(int i=0; i<50;i++) {
//			   String firstName=faker.name().firstName();
//			   String lastName=faker.name().lastName();
//			   String email=firstName+"."+lastName+"@gmail.com";
//			   int day = faker.number().numberBetween(10, 25);
//			   int month = faker.number().numberBetween(10, 12);
//			   int years = faker.number().numberBetween(2000, 2010);
//			   Department dpt = departmentRepo.findById(1).get();
//			   DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
//			   String date = day+"/"+month+"/"+years;
//
//			   LocalDate dob = LocalDate.parse(date, formatter);
//			   Student student = new Student(firstName, lastName, email,dob,dpt);
//			   studentRepo.save(student);
//		   }
//	   };
//
//	}
}
